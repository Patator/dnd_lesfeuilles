import os
import urllib.parse
from collections import defaultdict

# Base URL for the GitLab repository where the images are hosted
base_url = "https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/"
# Path to the folder containing images
base_folder_path    = "Images/Assets"
table_max_columns   = 5
table_column_width  = 200

def get_all_images(folder):
    """Recursively get all images in the specified folder and its subfolders."""

    images = []
    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.lower().endswith(('.jpg', '.jpeg', '.png', '.gif', '.webp')):
                images.append(os.path.join(root, file))

    return images


def create_image_struct(image_path, base_folder_path):
    """Create a struct for the given image."""

    relative_path   = os.path.relpath(image_path, base_folder_path)
    parts           = relative_path.split("/")
    file_name       = parts[-1]
    file_name_parts = file_name.split("#")
    name            = file_name_parts[-1].split(".")[0]
    sorting_list    = parts[:-1] + file_name_parts[:-1] + [name, file_name_parts[-1].split(".")[-1]]
    depth           = len(sorting_list) - 2
    
    struct = {
        "path":         relative_path,
        "name":         name,
        "sorting_list": sorting_list,
        "depth":        depth
    }
    
    # Print the struct
    #print("----")
    #print("Struct for", image_path)
    #for key, value in struct.items():
    #    print(f"{key}: {value}")

    return struct


def group_images(images, level=0, num_files_per_folder=-1):
    """Recursively group images by the element of the list at the current level position."""

    print("Processing images for level=", level)

    if not images:
        print("No images on this level, returning!!")
        return ""

    markdown_output  = ""
    grouped_images   = defaultdict(list)
    ungrouped_images = []

    # Get current group name
    group_name = images[0]["sorting_list"][level-1] if level != 0 else "Assets"
    
    print("Current group name=", group_name)

    # Print Header
    markdown_output += "#" * (level + 1) + " " + group_name + "\n\n"

    # Get images that are on the current level in print list and add others to group list
    for image in images:  

        image_level = len(image["sorting_list"]) - 2

        #print("Image:", image["path"])
        #print("      ", image["sorting_list"], " len:", image_level)

        # Image on current level
        if image_level == level:

            print("  --> Image on current level :", image["sorting_list"][level], "image Level:", image_level, "level:", level)
            
            ungrouped_images.append(image)

        # Level on higher level
        else:
            
            print("  --> Image on higher level  :", image["sorting_list"][level], "image Level:", image_level, "level:", level)            
            
            group_key = image["sorting_list"][level]
            grouped_images[group_key].append(image)

    # Print grouped images to table
    #print(" => Add", len(ungrouped_images), "images on current level to table:", ungrouped_images)
    markdown_output += print_image_table(ungrouped_images, num_files_per_folder)

    # Recurse to the groups
    #print(" => Recursing on groups:", grouped_images)
    for key, group in sorted(grouped_images.items()):

        print("Recursing on group:", key, "level old:", level, "level new:", level + 1)
        markdown_output += group_images(group, level + 1, num_files_per_folder)

    return markdown_output


def print_image_table(images, num_files_per_folder=-1):
    """Print a table with images."""
    
    if not images:
        print("# No images to add to table!")
        return ""

    # Sort images by name before generating the table
    images_sorted = sorted(images, key=lambda x: x["name"])

    table_output = "<table>\n  <tr>\n"

    for i, image in enumerate(images_sorted[:num_files_per_folder] if num_files_per_folder != -1 else images_sorted):
        if i % table_max_columns == 0 and i != 0:
            table_output += "  </tr>\n  <tr>\n"

        image_url = urllib.parse.urljoin(base_url, urllib.parse.quote(os.path.join(base_folder_path, image["path"])))
        table_output += f"    <td align='center'>\n"
        table_output += f"      <img src='{image_url}' alt='{image['name']}' width='{table_column_width}'/>\n"
        table_output += f"      <br/>\n"
        table_output += f"      {image['name']}\n"
        table_output += f"    </td>\n"

    # Debug prints
    #print("Last image:", image)
    #print("Image URL:", image_url)

    table_output += "  </tr>\n</table>\n\n"
    return table_output


# Get all images in all subfolders
all_images = get_all_images(base_folder_path)

# Create image structs
image_structs = [create_image_struct(image, base_folder_path) for image in all_images]

# Define how many files to keep per folder (or -1 for all)
num_files_per_folder = -1

# Group images recursively
markdown_output = group_images(image_structs, num_files_per_folder=num_files_per_folder)

# Write the Markdown content to a file
output_file_path = "images_list.md"
with open(output_file_path, "w") as output_file:
    output_file.write(markdown_output)

print("Markdown file generated successfully!")

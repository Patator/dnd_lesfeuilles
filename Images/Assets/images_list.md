# Assets

## Cartes

### Carte

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Cartes/Carte%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
  </tr>
</table>

## Locations

### Chemin côtier

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Chemin%20c%C3%B4tier%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Chemin%20c%C3%B4tier%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Chemin%20c%C3%B4tier%233.webp' alt='3' width='200'/>
      <br/>
      3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Chemin%20c%C3%B4tier%234.webp' alt='4' width='200'/>
      <br/>
      4
    </td>
  </tr>
</table>

### Chemin rivière

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Chemin%20rivi%C3%A8re%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Chemin%20rivi%C3%A8re%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
  </tr>
</table>

### Maison Canyon

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Maison%20Canyon%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
  </tr>
</table>

### Maison dans valée perchée

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Maison%20dans%20val%C3%A9e%20perch%C3%A9e%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Maison%20dans%20val%C3%A9e%20perch%C3%A9e%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Maison%20dans%20val%C3%A9e%20perch%C3%A9e%233.webp' alt='3' width='200'/>
      <br/>
      3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Maison%20dans%20val%C3%A9e%20perch%C3%A9e%234.webp' alt='4' width='200'/>
      <br/>
      4
    </td>
  </tr>
</table>

### Plage sauvage

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Plage%20sauvage%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
  </tr>
</table>

### Promontoire

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Promontoire%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Promontoire%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Promontoire%233.webp' alt='3' width='200'/>
      <br/>
      3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Promontoire%234.webp' alt='4' width='200'/>
      <br/>
      4
    </td>
  </tr>
</table>

### Promontoire paysage

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Promontoire%20paysage%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Promontoire%20paysage%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
  </tr>
</table>

### Ruine cotière

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%2310.webp' alt='10' width='200'/>
      <br/>
      10
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%233.webp' alt='3' width='200'/>
      <br/>
      3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%234.webp' alt='4' width='200'/>
      <br/>
      4
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%236.webp' alt='6' width='200'/>
      <br/>
      6
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%237.webp' alt='7' width='200'/>
      <br/>
      7
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%238.webp' alt='8' width='200'/>
      <br/>
      8
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20coti%C3%A8re%239.webp' alt='9' width='200'/>
      <br/>
      9
    </td>
  </tr>
</table>

### Ruine côtière

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20c%C3%B4ti%C3%A8re%2311.webp' alt='11' width='200'/>
      <br/>
      11
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20c%C3%B4ti%C3%A8re%2312.webp' alt='12' width='200'/>
      <br/>
      12
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20c%C3%B4ti%C3%A8re%2313.webp' alt='13' width='200'/>
      <br/>
      13
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20c%C3%B4ti%C3%A8re%2314.webp' alt='14' width='200'/>
      <br/>
      14
    </td>
  </tr>
</table>

### Ruine promontoire

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20promontoire%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20promontoire%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20promontoire%233.webp' alt='3' width='200'/>
      <br/>
      3
    </td>
  </tr>
</table>

### Ruine rivière

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20rivi%C3%A8re%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20rivi%C3%A8re%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20rivi%C3%A8re%233.webp' alt='3' width='200'/>
      <br/>
      3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20rivi%C3%A8re%234.jpg' alt='4' width='200'/>
      <br/>
      4
    </td>
  </tr>
</table>

### Ruine sur île

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Ruine%20sur%20%C3%AEle%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
  </tr>
</table>

### Tombe

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Tombe%231.webp' alt='1' width='200'/>
      <br/>
      1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Tombe%232.webp' alt='2' width='200'/>
      <br/>
      2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Tombe%233.webp' alt='3' width='200'/>
      <br/>
      3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Tombe%234.webp' alt='4' width='200'/>
      <br/>
      4
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Tombe%235.webp' alt='5' width='200'/>
      <br/>
      5
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Locations/Tombe%236.webp' alt='6' width='200'/>
      <br/>
      6
    </td>
  </tr>
</table>

## Objects Magiques

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Objects%20Magiques/Robe%20Prismatique.webp' alt='Robe Prismatique' width='200'/>
      <br/>
      Robe Prismatique
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Objects%20Magiques/Robe%20Prismatique2.webp' alt='Robe Prismatique2' width='200'/>
      <br/>
      Robe Prismatique2
    </td>
  </tr>
</table>

## PNJ

### Elfe

#### Femme

##### Scène 1

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%201%23Var%20a1.webp' alt='Var a1' width='200'/>
      <br/>
      Var a1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%201%23Var%20a2.webp' alt='Var a2' width='200'/>
      <br/>
      Var a2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%201%23Var%20a3.webp' alt='Var a3' width='200'/>
      <br/>
      Var a3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%201%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%201%23Var%20c.webp' alt='Var c' width='200'/>
      <br/>
      Var c
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%201%23Var%20d.webp' alt='Var d' width='200'/>
      <br/>
      Var d
    </td>
  </tr>
</table>

##### Scène 10

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2010%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
  </tr>
</table>

##### Scène 11

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2011%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 12

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2012%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 13

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2013%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 14

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2014%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 15

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2015%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 16

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2016%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 17

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2017%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2017%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
  </tr>
</table>

##### Scène 18

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2018%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 19

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2019%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 2

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%202%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 20

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2020%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 21

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2021%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 22

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2022%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 23

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2023%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 24

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2024%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 25

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%2025%23Var%20.jpg' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 3

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%203%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%203%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%203%23Var%20c.webp' alt='Var c' width='200'/>
      <br/>
      Var c
    </td>
  </tr>
</table>

##### Scène 4

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%204%23Var%20.webp' alt='Var ' width='200'/>
      <br/>
      Var 
    </td>
  </tr>
</table>

##### Scène 5

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%205%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%205%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%205%23Var%20c.webp' alt='Var c' width='200'/>
      <br/>
      Var c
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%205%23Var%20d.webp' alt='Var d' width='200'/>
      <br/>
      Var d
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%205%23Var%20e.webp' alt='Var e' width='200'/>
      <br/>
      Var e
    </td>
  </tr>
</table>

##### Scène 6

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%206%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%206%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%206%23Var%20c.webp' alt='Var c' width='200'/>
      <br/>
      Var c
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%206%23Var%20d.webp' alt='Var d' width='200'/>
      <br/>
      Var d
    </td>
  </tr>
</table>

##### Scène 7

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%207%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%207%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%207%23Var%20c.webp' alt='Var c' width='200'/>
      <br/>
      Var c
    </td>
  </tr>
</table>

##### Scène 8

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%208%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%208%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
  </tr>
</table>

##### Scène 9

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%209%23Var%20a.webp' alt='Var a' width='200'/>
      <br/>
      Var a
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/PNJ/Elfe%23Femme%23Sc%C3%A8ne%209%23Var%20b.webp' alt='Var b' width='200'/>
      <br/>
      Var b
    </td>
  </tr>
</table>

## Potions

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Potion%20of%20mind%20reading.webp' alt='Potion of mind reading' width='200'/>
      <br/>
      Potion of mind reading
    </td>
  </tr>
</table>

### Colorée 

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Blanche.webp' alt=' Blanche' width='200'/>
      <br/>
       Blanche
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Bleue%201.webp' alt=' Bleue 1' width='200'/>
      <br/>
       Bleue 1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Bleue%202.webp' alt=' Bleue 2' width='200'/>
      <br/>
       Bleue 2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Jaune.webp' alt=' Jaune' width='200'/>
      <br/>
       Jaune
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Multicolore.webp' alt=' Multicolore' width='200'/>
      <br/>
       Multicolore
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Orange.webp' alt=' Orange' width='200'/>
      <br/>
       Orange
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Rouge.webp' alt=' Rouge' width='200'/>
      <br/>
       Rouge
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Verte.webp' alt=' Verte' width='200'/>
      <br/>
       Verte
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20Violet.webp' alt=' Violet' width='200'/>
      <br/>
       Violet
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Potions/Color%C3%A9e%20%23%20%C3%89ther%C3%A9e.webp' alt=' Étherée' width='200'/>
      <br/>
       Étherée
    </td>
  </tr>
</table>

## Scrolls

<table>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Armure.webp' alt='Armure' width='200'/>
      <br/>
      Armure
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Champ%20anti-magie.webp' alt='Champ anti-magie' width='200'/>
      <br/>
      Champ anti-magie
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Changement%20de%20plan.webp' alt='Changement de plan' width='200'/>
      <br/>
      Changement de plan
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Charme.webp' alt='Charme' width='200'/>
      <br/>
      Charme
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Clignotement.webp' alt='Clignotement' width='200'/>
      <br/>
      Clignotement
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Communication%20avec%20les%20animaux.webp' alt='Communication avec les animaux' width='200'/>
      <br/>
      Communication avec les animaux
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Dissipation%20de%20la%20magie.webp' alt='Dissipation de la magie' width='200'/>
      <br/>
      Dissipation de la magie
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/D%C3%A9tection%20de%20la%20magie.webp' alt='Détection de la magie' width='200'/>
      <br/>
      Détection de la magie
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Flamme%20%C3%A9ternelle_1.webp' alt='Flamme éternelle_1' width='200'/>
      <br/>
      Flamme éternelle_1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Flamme%20%C3%A9ternelle_2.webp' alt='Flamme éternelle_2' width='200'/>
      <br/>
      Flamme éternelle_2
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Folie.webp' alt='Folie' width='200'/>
      <br/>
      Folie
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Invisibilit%C3%A9_1.webp' alt='Invisibilité_1' width='200'/>
      <br/>
      Invisibilité_1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Invisibilit%C3%A9_2.webp' alt='Invisibilité_2' width='200'/>
      <br/>
      Invisibilité_2
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Invocation%20Mort-Vivant.webp' alt='Invocation Mort-Vivant' width='200'/>
      <br/>
      Invocation Mort-Vivant
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Lecture%20des%20pens%C3%A9es.webp' alt='Lecture des pensées' width='200'/>
      <br/>
      Lecture des pensées
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Localisation%20d%27objets.webp' alt='Localisation d'objets' width='200'/>
      <br/>
      Localisation d'objets
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Lumi%C3%A8res%20danssantes.webp' alt='Lumières danssantes' width='200'/>
      <br/>
      Lumières danssantes
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Obscurit%C3%A9.webp' alt='Obscurité' width='200'/>
      <br/>
      Obscurité
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Parler%20avec%20les%20morts.webp' alt='Parler avec les morts' width='200'/>
      <br/>
      Parler avec les morts
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Pluie%20de%20feu.webp' alt='Pluie de feu' width='200'/>
      <br/>
      Pluie de feu
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Projectile%20magique.webp' alt='Projectile magique' width='200'/>
      <br/>
      Projectile magique
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Protection%20contre%20la%20mort.webp' alt='Protection contre la mort' width='200'/>
      <br/>
      Protection contre la mort
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Protection%20contre%20le%20mal%20et%20le%20bien.webp' alt='Protection contre le mal et le bien' width='200'/>
      <br/>
      Protection contre le mal et le bien
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Respiration%20Aquatique_1.webp' alt='Respiration Aquatique_1' width='200'/>
      <br/>
      Respiration Aquatique_1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Respiration%20Aquatique_2.webp' alt='Respiration Aquatique_2' width='200'/>
      <br/>
      Respiration Aquatique_2
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Simulacre%20de%20vie.webp' alt='Simulacre de vie' width='200'/>
      <br/>
      Simulacre de vie
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Soins%20maladies.webp' alt='Soins maladies' width='200'/>
      <br/>
      Soins maladies
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Sommeil.webp' alt='Sommeil' width='200'/>
      <br/>
      Sommeil
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Terreur_1.webp' alt='Terreur_1' width='200'/>
      <br/>
      Terreur_1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Terreur_2.webp' alt='Terreur_2' width='200'/>
      <br/>
      Terreur_2
    </td>
  </tr>
  <tr>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Terreur_3.webp' alt='Terreur_3' width='200'/>
      <br/>
      Terreur_3
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Vent%20protecteur.webp' alt='Vent protecteur' width='200'/>
      <br/>
      Vent protecteur
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Vol_1.webp' alt='Vol_1' width='200'/>
      <br/>
      Vol_1
    </td>
    <td align='center'>
      <img src='https://gitlab.com/Patator/dnd_lesfeuilles/-/raw/main/Images/Assets/Scrolls/Vol_2.webp' alt='Vol_2' width='200'/>
      <br/>
      Vol_2
    </td>
  </tr>
</table>


# Images in Folder

Below are the images found in the specified folder:

<table>
  {% for row in images|batch(4) %}
    <tr>
      {% for image in row %}
        <td align="center">
          <img src="{{ image }}" alt="{{ image }}" width="200"/>
          <br/>
          {{ image }}
        </td>
      {% endfor %}
    </tr>
  {% endfor %}
</table>

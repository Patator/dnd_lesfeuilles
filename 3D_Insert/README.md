# Insert pour cofret métalique

Petit insert pratique pour y ranger dés, figurine et cartes de sorts.

![Vue d'ensemble](img/01_Ensemble.jpg)

Inutile, donc indispensable: l'insert D&D! :)

![Vue raprochée](img/02_Insert.jpg)

## Fixations

Les fixations servent à garder au plat le plateau en Plexy et à retenir les cartes de sorts pour qu'elles ne tombent pas lors du transport/vidage:

![Boîte ouverte #1](img/03_Ouvert_1.jpg)

![Boîte ouverte #1](img/04_Ouvert_2.jpg)

Aussi, avec les fixations en place, il est possible de trier les cartes en trois lots (par exemple: sorts préparés, sorts connus, autres sorts dispo chez le paladin ou sorts de parchemins, ...):

![Exemple avec cartes](img/05_Cartes.jpg)

## Paramètres Cura

### Add-on "Arc Welding":
- Active l'addon: permet d'avoir des puits pour le D20 et le DdV plus propres, surtout si OctoPrint galère à envoyer assez vite les Ordres
- Rayon maximum: 100 (évite d'avoir des bosses dans les murs à cause d'Arcs mal placé)

### Autres:
- Infill: 15%
- Infill mode: Gyroide
- Ironing: dernier niveau seulement (sinon sous-extrusion possible après le repassage...)

### Fixations:

- Première couche: 0.24 mm
- Couches suivantes: 0.10 mm

*Note:* imprimer les fixations sur le dos et surtout en imprimer plusieurs à la fois, permet d'obtenir un meilleur ergot.

![Fixations dans Cura](img/08_Fixations.png)

## Plus de vues:

![Vue Cura](img/06_Cura.png)

![Solid Edge](img/07_CAD.png)




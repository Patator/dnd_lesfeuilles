# Chevalet pour écran de maître et autre

## Description

Ces chevalet sont à imprimer de préférence à l'aide d'une imprimante résine en ABS ou mieux Nylon-like.

Lien vers le [modèle 3D](/Chevalet/Chevalet/Chevalet_2a.stl)


![Chevalet #1](/Chevalet/Chevalet_1.jpg)
![Chevalet #1](/Chevalet/Chevalet_2.jpg)

Une versions très préliminaire de "porte chevalet" pour les organiser:

Lien vers le [modèle 3D](/Chevalet/Base_de_Support.stl)

![Support #1](/Chevalet/Support_1.jpg){width=360px}
![Support #2](/Chevalet/Support_2.jpg){width=360px}
![Support #3](/Chevalet/Support_3.jpg){width=360px}

## Version Présupportées

Voici les versions des modèles 3D présupportés:

![Chevalet présupporté](/Chevalet/Chevalet_a2_pre.png){width=360px}
[Chevalet](/Chevalet/Chevalet_2a_pre.stl)

![Base présupportée](/Chevalet/Base_de_Support_pre.png){width=360px}
[Base](/Chevalet/Base_de_Support_pre.stl)

## Impression

Pour imprimer correctement les chevalets filigranes, placer ceux-ci verticalement comme dans l'exemple suivant:

![Exemple Impression](/Chevalet/Print_Layout.png)

## Exemple

Exemples utilisant les cartes de créature:

![Exemple #1](/Chevalet/Exemple_1.jpg)
![Exemple #2](/Chevalet/Exemple_2.jpg)
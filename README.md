# DnD_LesFeuilles

Juste quelques feuilles pour jouer à Donjons et Dragons old school avec papier/crayon!

**Feuilles PJ et MD:**

![](/Overview.png)

**Feuilles Info PNJ:**

![](/Overview%20-%20NPC%20-%20Livret.png)

![](/Overview%20-%20NPC%20-%20Fiche.png)

**Feuilles Rencontres Simplifiées:**

![](/Overview_-_Rencontre_G%C3%A9n%C3%A9rique.png)

**Couverture pour Pochette Rigide**

![](/Overview_-_Hardcover_Book.png)

## Couverture Dossier de Personnage

- [Couverture](/PJ_-_CoverSheet.svg)

Un "grand merci"/"big thanks" aux auteurs (Captdiablo, DeesWrld86, Sacred-Templar) de ces belles images de perso et les sceaux afin de me permettre de les placer ici.


## Feuille PJ

- (v3) [Feuille Principale](/PJ_-_Principale_v3.png) / [Version Parchemin](/PJ_-_Principale_v3_Parchemin.png)

![Feuille Principale](/PJ_-_Principale_v3.png){width=360px} ![Version Parchemin](/PJ_-_Principale_v3_Parchemin.png){width=360px}

- (legacy) [Feuille Principale - v1](/PJ_-_Principale_v1.png) (plus de place pour les aptitudes et résistances condensées)

![Feuille Principale - v1](/PJ_-_Principale_v1.png){width=360px}

- (legacy) [Feuille Principale - v2](/PJ_-_Principale_v2.png) (encore plus de place pour les aptitudes et résistances réduites au minimum)

![Feuille Principale - v2](/PJ_-_Principale_v2.png){width=360px}


### Feuille PJ (v3) pour PJ Multiclassé

- [Feuille Principale](/PJ_-_Principale_v3_MC) / [Version Parchemin](/PJ_-_Principale_v3_MC_Parchemin.png)

![Feuille Principale - v3](/PJ_-_Principale_v3_MC.png){width=360px} ![Version Parchemin](/PJ_-_Principale_v3_MC_Parchemin.png){width=360px}

## Feuilles Comparses pour PJ

Les règles des sidekicks de l'Unearthed Arcana (UA) se trouvent sur le site de [WotC](https://media.wizards.com/2018/dnd/downloads/UA_Sidekicks.pdf).

Les feuilles suivantes sont basée sur le TCoE (Chaudron des Merveilles de Tasha) et sont applicables aux comparses du Kit Essentiel.

- [Comparse Simplifé](/PNJ_-_Comparse_Simplifié.png) et [version améliorée](/PNJ_-_Comparse_Simplifié_v2.png)

![Comparse Simplifé](/PNJ_-_Comparse_Simplifié.png){width=360px} ![version améliorée](/PNJ_-_Comparse_Simplifié_v2.png){width=360px}

- [Comparse (TCoE)](/PNJ_-_Comparse_TCoE.png) et [version améliorée](/PNJ_-_Comparse_TCoE_v2.png)

![Comparse (TCoE)](/PNJ_-_Comparse_TCoE.png){width=360px} ![version améliorée](/PNJ_-_Comparse_TCoE_v2.png){width=360px}

Le comparse animal est une variante pour le cas bien particulier des animaux et autres créatures et est basé sur le compagnon d'armes:

- [Comparse Animal](/PNJ_-_Comparse_Animal.png) et [version améliorée](/PNJ_-_Comparse_Animal_v2.png)

![Comparse Animal](/PNJ_-_Comparse_Animal.png){width=360px} ![version améliorée](/PNJ_-_Comparse_Animal_v2.png){width=360px}

## Feuilles Historique, Traits, Dons et Aptitudes

- [Historique et Aptitudes](/PJ_-_Historique_et_Aptitudes.png) / [Version Parchemin](/PJ_-_Historique_et_Aptitudes_Parchemin.png)

![Historique et Aptitudes](/PJ_-_Historique_et_Aptitudes.png){width=360px} ![Version Parchemin](/PJ_-_Historique_et_Aptitudes_Parchemin.png){width=360px}

- [Historique et Aptitudes (ext.)](/PJ_-_Historique_et_Aptitudes_Ext.png) / [Version Parchemin](/PJ_-_Historique_et_Aptitudes_Ext_Parchemin.png)

![Historique et Aptitudes (ext.)](/PJ_-_Historique_et_Aptitudes_Ext.png){width=360px} ![Version Parchemin](/PJ_-_Historique_et_Aptitudes_Ext_Parchemin.png){width=360px}

## Feuilles Magie

- [Sorts étendus (principale)](/PJ_-_Sorts_-_Principale.png) / [Version Parchemin](/PJ_-_Sorts_-_Principale_Parchemin.png)

![Sorts étendus (principale)](/PJ_-_Sorts_-_Principale.png){width=360px} ![Version Parchemin](/PJ_-_Sorts_-_Principale_Parchemin.png){width=360px}

- [Sorts étendus](/PJ_-_Sorts.png) / [Version Parchemin](/PJ_-_Sorts_Parchemin.png)

![Sorts étendus](/PJ_-_Sorts.png){width=360px} ![Version Parchemin](/PJ_-_Sorts_Parchemin.png){width=360px}

## Feuilles d'Inventaires

- [Inventaire](/PJ_-_Inventaire_-_Général.png) / [Version Parchemin](/PJ_-_Inventaire_-_Général_Parchemin.png)

![Inventaire](/PJ_-_Inventaire_-_Général.png){width=360px} ![Version Parchemin](/PJ_-_Inventaire_-_Général_Parchemin.png){width=360px}

- [Équipement - Version Éditable](/PJ_-_Inventaire_-_%C3%89quipement.svg) / [fiche d'équipement](/PJ_-_Inventaire_-_%C3%89quipement_Vierge.png)

![Équipement - Version Éditable](/PJ_-_Inventaire_-_%C3%89quipement.svg){width=360px} ![fiche d'équipement](/PJ_-_Inventaire_-_%C3%89quipement_Vierge.png){width=360px}

- [Équipement (F)](/PJ_-_Inventaire_-_Équipement_-_F.png) / [Version Parchemin](/PJ_-_Inventaire_-_Équipement_-_F_Parchemin.png)

![Équipement (F)](/PJ_-_Inventaire_-_Équipement_-_F.png){width=360px} ![Version Parchemin](/PJ_-_Inventaire_-_Équipement_-_F_Parchemin.png){width=360px}

- [Équipement (M)](/PJ_-_Inventaire_-_Équipement_-_M.png) / [Version Parchemin](/PJ_-_Inventaire_-_Équipement_-_M_Parchemin.png)

![Équipement (M)](/PJ_-_Inventaire_-_Équipement_-_M.png){width=360px} ![Version Parchemin](/PJ_-_Inventaire_-_Équipement_-_M_Parchemin.png){width=360px}

- [Objets Magiques](/PJ_-_Inventaire_-_Objets_Magiques.png) / [Version Parchemin](/PJ_-_Inventaire_-_Objets_Magiques_Parchemin.png)

![Objets Magiques](/PJ_-_Inventaire_-_Objets_Magiques.png){width=360px} ![Version Parchemin](/PJ_-_Inventaire_-_Objets_Magiques_Parchemin.png){width=360px}

- [Objets Magiques avec Sorts](/PJ_-_Inventaire_-_Objets_Magiques_-_Sorts.png) / [Version Parchemin](/PJ_-_Inventaire_-_Objets_Magiques_-_Sorts_Parchemin.png)

![Objets Magiques avec Sorts](/PJ_-_Inventaire_-_Objets_Magiques_-_Sorts.png){width=360px} ![Version Parchemin](/PJ_-_Inventaire_-_Objets_Magiques_-_Sorts_Parchemin.png){width=360px}

Divers:
- [Coffre (aux trésors)](/PJ_-_Inventaire_-_Coffre.png) / [Version Parchemin](/PJ_-_Inventaire_-_Coffre_Parchemin.png)

![Coffre (aux trésors)](/PJ_-_Inventaire_-_Coffre.png){width=360px} ![Version Parchemin](/PJ_-_Inventaire_-_Coffre_Parchemin.png){width=360px}


## Feuilles du Maître

Groupe de PJ:

- [Suivit de Groupe](/MD_-_Stats_de_Groupe.png)

![Suivit de Groupe](/MD_-_Stats_de_Groupe.png){width=360px}


Rencontres:

- [Rencontre - Nombreuse #1](/MD_-_Rencontre_-_Nombreuse.png)

![Rencontre - Nombreuse #1](/MD_-_Rencontre_-_Nombreuse.png){width=360px}

Rencontres - version simplifée:

- [Rencontre Simplifiée](/MD_-_Rencontre_-_G%C3%A9n%C3%A9rique.png)

![Rencontre Simplifiée](/MD_-_Rencontre_-_G%C3%A9n%C3%A9rique.png){width=360px}

- [Rencontre Simplifiée avec États](/MD_-_Rencontre_-_G%C3%A9n%C3%A9rique%2B%C3%89tats.png)

![Rencontre Simplifiée avec États](/MD_-_Rencontre_-_G%C3%A9n%C3%A9rique%2B%C3%89tats.png){width=360px}

- [Rencontre Simplifiée - tours suplémentaires](/MD_-_Rencontre_-_G%C3%A9n%C3%A9rique_%C3%89tendue.png)

![Rencontre Simplifiée - tours suplémentaires](/MD_-_Rencontre_-_G%C3%A9n%C3%A9rique_%C3%89tendue.png){width=360px}

Rencontres - version simplifée avec notes d'états:

- [Rencontre Simplifiée avec notes](/MD_-_Rencontre_-_Générique+Notes.png) / [Version Parchemin](/MD_-_Rencontre_-_Générique+Notes_Parchemin.png)

![Rencontre Simplifiée avec notes](/MD_-_Rencontre_-_Générique+Notes.png){width=360px} ![Version Parchemin](/MD_-_Rencontre_-_Générique+Notes_Parchemin.png){width=360px}

- [Rencontre Simplifiée avec notes et États](/MD_-_Rencontre_-_Générique+Notes+États.png) / [Version Parchemin](/MD_-_Rencontre_-_Générique+États+Notes_Parchemin.png)

![Rencontre Simplifiée avec notes et États](/MD_-_Rencontre_-_Générique+Notes+États.png){width=360px} ![Version Parchemin](/MD_-_Rencontre_-_Générique+États+Notes_Parchemin.png){width=360px}

Résumé PNJ au format "Fiche":

- [Fiche - Couverture](/MD_-_NPC_-_Fiche_Recto.svg) ou version simple [image transparente](/MD_-_NPC_-_Fiche_Recto_Base.png)

![image transparente](/MD_-_NPC_-_Fiche_Recto_Base.png){width=360px}

- [Fiche - Verso pour MD](/MD_-_NPC_-_Fiche_Verso_-_MD.png)

![Fiche - Verso pour MD](/MD_-_NPC_-_Fiche_Verso_-_MD.png){width=360px}

- [Fiche - Verso pour PJ](/MD_-_NPC_-_Fiche_Verso_-_PJ.png)

![Fiche - Verso pour PJ](/MD_-_NPC_-_Fiche_Verso_-_PJ.png){width=360px}

Résumé PNJ au format "Livret":

- [Livret - Couverture](/MD_-_NPC_-_Livret_Couverture.svg) ou version simple [image transparente](/MD_-_NPC_-_Livret_Couverture_Base.png)

![image transparente](/MD_-_NPC_-_Livret_Couverture_Base.png){width=360px}

- [Livret - Intérieur pour MD](/MD_-_NPC_-_Livret_Int%C3%A9rrieur_-_MD.png)

![Livret - Intérieur pour MD](/MD_-_NPC_-_Livret_Int%C3%A9rrieur_-_MD.png){width=360px}

- [Livret - Intérieur pour PJ](/MD_-_NPC_-_Livret_Int%C3%A9rrieur_-_PJ.png)

![Livret - Intérieur pour PJ](/MD_-_NPC_-_Livret_Int%C3%A9rrieur_-_PJ.png){width=360px}

## Accessoires

Porte fiches cartoné à trois volets:

- [Porte Fiches - SVG éditable](/Hardcover_Book.svg)
- [Porte Fiches - Plan d'impression, découpe et collage dossier de fiches](/Overview_-_Hardcover_Book_-_Cutsheet.png)

![Porte Fiches - Plan d'impression, découpe et collage dossier de fiches](/Overview_-_Hardcover_Book_-_Cutsheet.png){width=360px}

![](/Overview_-_Hardcover_Book_-_Cutsheet.png)



![](https://visitor-badge.glitch.me/badge?page_id=dnd_lesfeuilles)

# Crédits

Rendons à César ce qui lui appartient: merci pour les icônes à https://game-icons.net/ (CC-BY 3.0) ainsi qu'à https://svgsilh.com/ (CC0) pour certaines images et https://www.svgrepo.com/ (CC0) pour des icônes.

WotC Fan content Policy: https://company.wizards.com/en/legal/fancontentpolicy

“DnD LesFeuilles is unofficial Fan Content permitted under the Fan Content Policy. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.”

Images Perso:

- Captdiablo: https://www.deviantart.com/captdiablo/art/Mage-897055206 (CC-BY-NC-ND)
- Sacred-Templar: https://www.deviantart.com/sacred-templar/art/Witcher-890317380 (CC-BY)
- Sacred-Templar: https://www.deviantart.com/sacred-templar/art/me-786380612 (CC-BY)
- Sacred-Templar: https://www.deviantart.com/sacred-templar/art/lady-and-a-creature-794943527 (CC-BY)
- Diedo20: https://pixabay.com/de/illustrations/abenteuer-alt-drachen-rpg-fantasie-7393801/ (Pixabay License)
- Profileiche: https://pixabay.com/de/illustrations/frau-jung-charakter-rollenspiel-7705310/ (Pixabay License)

Sceaux:
- DeesWrld86: https://www.deviantart.com/deeswrld86/gallery/75474713/roleplay-papers-and-seals-2
